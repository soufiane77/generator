#!/bin/bash

####################################
##
##  Generateur de docker-compose
##
####################################


## Variables ####################################################

DIR="${HOME}/generator"
USER_SCRIPT=${USER}

## Functions ####################################################

help(){

echo "USAGE :

  ${0##*/} [-h] [--help]
  
  Options :

    -h, --help : aides

    -p, --postgres : lance une instance postgres

    -i, --ip : affichage des ip

"
}

ip() {
for i in $(docker ps -q); do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;done
}


parser_options(){

case $@ in	
	-h|--help)
	  help
	;;

	-p|--postgres)
	  postgres
	;;

	*)
        echo "option invalide, lancez -h ou --help"
esac
}

postgres(){

echo ""
echo "Installation d'une instance postgres..."
echo ""
echo "1 - Création du répertoire de datas.."
mkdir -p $DIR/postgres
echo ""
echo "
version: '2'
services:
  db:
    image: mariadb:latest
    container_name: mariadb
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: soufiane
      MYSQL_DATABASE: spring-reddit-api
    volumes:
      - './src/main/resources/data.sql:/tmp/data.sql:ro'
      - './src/main/resources/schema.sql:/tmp/schema.sql:ro'
    networks:
    - generator


networks:
  generator:
    driver: bridge
    ipam:
      config:
        - subnet: 192.169.0.0/24


" >$DIR/docker-compose-mysql.yml

echo "2 - Run de l'instance..."
docker-compose -f $DIR/docker-compose-mysql.yml  up -d

echo ""
echo "
Credentials :
    - PORT : 3306
    - MYSQL_USER: root
    - MYSQL_PASSWORD: soufiane

Command : mysql -h <ip> -u myuser
"
}




## Execute ######################################################

parser_options $@
ip
